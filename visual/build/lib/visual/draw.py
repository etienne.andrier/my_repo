import cv2
import numpy as np
import visual.filter as f


def draw_segments(img, lines, color, thickness):
    #draws a series of segments in the img file,
    #img : an image, type : numpy array
    #lines : a list of lines
    #color : a color gammut, RGB format
    for line in lines:
        start = (int(line[0]),int(line[1]))
        finish = (int(line[2]),int(line[3]))
        cv2.line(img, start, finish,color,thickness)

def draw_line(img, line, color, thickness):
    #draws a series of lines in the img file,
    #img : an image, type : numpy array
    #img : a path name to an image file
    #lines : a list of lines
    #color : a color gammut, BGR format
    dimensions = img.shape
    #if not line_in_frame(line,img):
    #    raise Exception("The line is not within the frame of the image")
    segment_points = maximum_segment(line,img)
    if len(segment_points)==4:
        start = (int(segment_points[0]),int(segment_points[1]))
        finish = (int(segment_points[2]),int(segment_points[3]))
        cv2.line(img, start, finish, color, thickness)    

def draw_lines(img, lines, color, thickness):
    #draws a series of lines in the img file,
    #img : an image, type : numpy array
    #img : a path name to an image file
    #lines : a list of lines
    #color : a color gammut, RGB format
    for line in lines:
        if len(line)!=0:
            draw_line(img, line, color, thickness)

def line_in_frame(line,img):
    #returns True if the line is within the frame
    #of the image, False if not
    dimensions = img.shape
    h = dimensions[0] #height of the picture
    l = dimensions[1] #width of the picture
    a = line[4]
    b = line[5]
    c = line[6]
    def d(x,y):
        return a*x + b*y + c
    return d(0,0)*d(0,h)<=0 or d(0,0)*d(l,0)<=0 or d(0,h)*d(l,h)<=0 or d(l,0)*d(l,h)<=0
           
"""def maximum_segment(line,img):
    dimensions = img.shape
    h = dimensions[0] #height of the picture
    l = dimensions[1] #width of the picture
    lines = np.array([[0,0,l,0,0,1,0,l],[0,0,0,h,1,0,0,h],[0,0,l,0,0,1,-h,l],[0,0,0,h,1,0,-l,h],line]) #top, left, bottom, right
    inters = f.intersect(lines)
    inters = inters[ (inters[:,0]>=0) & (inters[:,0]<=h) & (inters[:,1]>=0) & (inters[:,1]<=l)]
    result = np.array([])
    for i in range len(inters):
        if inters[i]!=np.array([0,0]) and inters[i]!=np.array([0,h]) and inters[i]!=np.array([l,0]) and inters[i]!=np.array([0,0]):
    return inters
"""

def maximum_segment(line,img):
    #returns the longest segment that aligns with the line,
    #considering the dimensions of the image
    #if not line_in_frame(line,img):
    #    raise Exception("The line is not within the frame of the image")
    dimensions = img.shape
    h = dimensions[0] #height of the picture
    l = dimensions[1] #width of the picture
    a = line[4]
    b = line[5]
    c = line[6]
    def d(x,y):
        return a*x + b*y + c
    def has_inters(x1,y1,x2,y2):
        return d(x1,y1)*d(x2,y2)<=0
    intersections = [has_inters(0,0,0,h),
                    has_inters(0,0,l,0),
                    has_inters(0,h,l,h),
                    has_inters(l,0,l,h)] #in order : left, top, bottom, right sections of the frame
    #intersections now contains "1" at the positions that correspond to where the line intersects with the frame of the image
    points = []
    if a!=0 and b!=0:
        if intersections[0]:
            points.append(0)
            points.append(-c/b)
        if intersections[1]:
            points.append(-c/a)
            points.append(0)
        if intersections[2]:
            points.append(-(b*h+c)/a)
            points.append(h)
        if intersections[3]:
            points.append(l)
            points.append(-(c+a*l)/b)
    return np.array(points)

def test():
    path= r"C:\users\student\2021andrieret\bureau\geeks14.png"
    image=cv.imread(path)
    start_point = (-125,125)
    end_point=(250,250)
    color = (0,255,0)
    thickness = 6
    image = cv.line(image, start_point, end_point, color, thickness)
    cv.imshow("image",image)

