import numpy as np
import sys

def longer_than(lines,min_length):
    new_lines=[]
    for line in lines:
        if line[-1]>min_length:
            new_lines.append(line)
    return new_lines

def remove_small_lines(lines,img):
    dimensions = img.shape
    lines = lines[lines[:,7]>dimensions[0]/8]
    return lines

def near_angle(lines,theta,sensibility):
    ab = lines[...,4:6]
    point = np.array([np.cos(theta),np.sin(theta)])
    d2 = np.sum((ab - point)**2, axis=1)
    return lines[d2<sensibility**2]

def far_from_angle(lines,theta,sensibility):
    ab = lines[...,4:6]
    point = np.array([np.cos(theta),np.sin(theta)])
    d2 = np.sum((ab - point)**2, axis=1)
    return lines[d2>sensibility**2]

def remove_horizontals_verticals(lines):
    lines = far_from_angle(lines,0,0.1)
    lines = far_from_angle(lines,np.pi/2,0.1)
    lines = far_from_angle(lines,-np.pi/2,0.1)
    return lines

def mean_lines(lines):
    right = near_angle(lines,np.pi/4,0.5)
    left = near_angle(lines,3*np.pi/4,0.5)
    if len(right)==0 or len(left)==0:
        return [[],[]]
    right = np.mean(right,axis=0)
    left = np.mean(left,axis=0)
    return [right,left]

def angle_x(line):
    #retourne l'angle en radians entre la ligne et l'axe des abscisses x
    a = line[4]
    b = line[5]
    theta = np.arctan(b/a)
    return abs(theta)

"""
def remove_horizontals_verticals(lines):
    ab = np.absolute(lines[:,4:6])
    n = ab.shape[0]
    sensibility = np.repeat(0.1,n)
    hor = np.repeat(np.array([[1,0]]),n,axis = 0)
    distHor = np.square(ab - hor)
    distHor = np.sum(distHor,axis = 1)
    lines = lines[distHor > sensibility]

    aabb = np.absolute(lines[:,4:6])
    n = aabb.shape[0]
    sensibilityy = np.repeat(0.1,n)
    ver = np.repeat(np.array([[0,1]]),n,axis = 0)
    distVer = np.square(aabb - ver)
    distVer = np.sum(distVer,axis = 1)
    lines = lines[distVer > sensibilityy]
    return lines"""

"""def remove_horizontals_verticals(lines):
    lines = lines[lines[:,4]!=0]
    lines = lines[lines[:,5]!=0]
    a = lines[:,4]
    b = lines[:,5]
    hor = np.absolute(a/b)
    sensibility = np.repeat(0.1,len(lines))
    lines = lines[hor>sensibility]

    a = lines[:,4]
    b = lines[:,5]
    ver = np.absolute(b/a)
    sensibility = np.repeat(0.1,len(lines))
    lines = lines[ver>sensibility]

    return lines
"""
def intersect(lines):
    intersections = []
    for i, si in enumerate(lines):
        for sj in lines[i+1:]:
            cross_product = np.cross(si[4:6], sj[4:6]) # [a1,b1] ^ [a2, b2]
            if cross_product != 0:
                coeff = 1.0 / cross_product

                intersections.append([coeff * np.cross(si[5:7]   , sj[5:7]), # [b1, c1] ^ [b2, c2]
                                      coeff * np.cross(sj[[4, 6]], si[[4, 6]])]) # -[a1, c1] ^ [a2, c2]
    return np.array(intersections)

def sci(values, confidence) :
    """
    values : an array of scalars (e.g. [1.2, 100.6, 23.78, ....])
    confidence : in [0,1], (e.g 0.5 for 50%)
    """
    nb        = values.shape[0]
    values    = np.sort(values)
    size      = (int)(nb*confidence+.5)
    nb_iter   = nb - size + 1
    sci       = None
    sci_width = sys.float_info.max
    inf       = 0
    sup       = size
    for i in range(nb_iter) :
        sciw = values[sup-1] - values[inf]
        if sciw < sci_width :
            sci       = values[inf:sup]
            sci_width = sciw
        inf += 1
        sup += 1
    # The result is the array (ordered) of the values inside the sci.
    return sci


def complete_filter(lines,img):
    #synthesis of all the filtering methods above
    lines = remove_small_lines(lines,img)
    lines = remove_horizontals_verticals(lines)
    return lines

