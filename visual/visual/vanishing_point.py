import cv2
import numpy as np
import visual.draw as d
import visual.filter as f
import sys

def line_detection(img):
    #Returns a list of lines detected in the image 'img' (a numpy array)
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    lsd_detector = cv2.ximgproc.createFastLineDetector()
    lines = lsd_detector.detect(gray_img)
    lines = np.squeeze(lines)
    lines_good_format = []
    if type(lines.shape) != tuple:
        return lines_good_format
    if len(lines.shape) != 2:
        return lines_good_format
    else:
        x0 = lines[:,0]
        y0 = lines[:,1]
        x1 = lines[:,2]
        y1 = lines[:,3]
        a = y0 - y1
        b = x1 - x0
        a[b<0]*=-1
        b[b<0]*=-1
        N = (a**2 + b**2)**0.5
        a = a/N
        b = b/N
        c = -(a*x0 + b*y0)
        n = ((y1-y0)**2 + (x1 - x0)**2)**0.5
        lines_good_format = np.column_stack((x0,y0,x1,y1,a,b,c,n))
    return lines_good_format

def vanishing_pt(img):
    lines = line_detection(img)
    if lines==[]:
        return 100,img,100  
    lines = f.complete_filter(lines,img)
    right,left = f.mean_lines(lines)
    d.draw_lines(img,[left],(0,0,255),2)
    d.draw_lines(img,[right],(0,255,0),2)
    height = img.shape
    v_point = []

    def intersect(lines):
        if len(lines[0])==0:
            return []
        intersections = []
        for i, si in enumerate(lines):
            for sj in lines[i+1:]:
                cross_product = np.cross(si[4:6], sj[4:6]) # [a1,b1] ^ [a2, b2]
                if cross_product != 0:
                    coeff = 1.0 / cross_product
                    intersections.append([coeff * np.cross(si[5:7]   , sj[5:7]), # [b1, c1] ^ [b2, c2]
                                        coeff * np.cross(sj[[4, 6]], si[[4, 6]])]) # -[a1, c1] ^ [a2, c2]
        return np.array(intersections)
    #en utilisant sci pour filtrer les intersections : 
    """intersections = intersect(lines)
    if len(intersections)>5:
        filtered_x = f.sci(intersections[:,0],0.1)
        filtered_y = f.sci(intersections[:,1],0.1)
        v_point = [(filtered_x[0]+filtered_x[-1])/2,(filtered_y[0]+filtered_y[-1])/2]
        img = cv2.circle(img, (int(v_point[0]),int(v_point[1])), 5, (0,255,0), 4)"""
    intersections = intersect([right,left])
    #calcul du décalage :
    unbalance = 0
    if len(right)!=0 and len(left)!=0:
        unbalance = f.angle_x(left)-f.angle_x(right)

    if len(intersections)==1:
        v_point = intersections[0] #avec 2 lignes il n'y a qu'une intersection
        img = cv2.circle(img, (int(v_point[0]),int(v_point[1])), 5, (0,255,0), 4)

    if len(v_point)==0:#si on ne détecte pas de vanishing point, la valeur du déséquilibre d'angle et celle de la hauteur normalisée
    #du vanishing point sont élevées à 100
        return 100,img,100

    width = img.shape[1]
    vp_x = (v_point[0]/width)-0.5 #le décalage du vp compris entre -0.5 et 0.5
    return vp_x, img, unbalance




if __name__ == '__main__':
    # Execute when the module is not initialized from an import statement.
    """coord = vanishing_pt(image)
    coords = (int(coord[0]),int(coord[1]))
    image = cv2.circle(image, coords, 5, (0,255,0), 4)
    cv2.imwrite("VP.png",image)"""
    a = np.array([393.61473127,391.84305631],[311.30618095,311.30618095])
    print(a[a[:,1]<350])

