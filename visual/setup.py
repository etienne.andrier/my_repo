#!/usr/bin/env python3

from setuptools import setup

setup(name='visual',
      version='0.1',
      description='A visual processing library',
      url='',
      author='',
      author_email='',
      license='MIT',
      packages=['visual'],
      zip_safe=False)