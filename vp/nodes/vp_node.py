#!/usr/bin/env python3

import rospy
import visual as vs

# import the message type you need (here are examples)
from std_msgs.msg      import Float32, Empty
from sensor_msgs.msg   import CompressedImage
from geometry_msgs.msg import Twist

# If you need to protect a variable by a mutex...
from multiprocessing import Lock

# Then, as usual
import numpy as np

# And for image processing
import cv2
import visual as vs


class MyNode :

    def __init__(self) : # We are in thread #1 here.

        # let us declare attributes that can be retrieved easily in the different methods.
        
        self.data = None         # this will be thread protected...
        self.data_mutex = Lock() # ... thanks to this mutex.

        # Let us define the publishers
        self.pub_image = rospy.Publisher("/image_out_vp/compressed", CompressedImage, queue_size = 1)
        self.pub_vp = rospy.Publisher("/vanishing_point_y", Float32, queue_size = 1)
        self.pub_centre = rospy.Publisher("/centrism", Float32, queue_size = 1)
        self.pub_ratio = rospy.Publisher("/ratio",                Float32,         queue_size = 1)

        # Let us define some subscribers. Each declaration created a thread.
        
        self.sub_image = rospy.Subscriber("bebop/image_raw/compressed",  CompressedImage,
                                          self.on_image,   # This is thread #2
                                          queue_size = 1, buff_size=2**22) # This buff_size tricks prevents from delay in image stream.

    # This called within thread #2 when an image message is recieved.
    def on_image(self, msg):
        # From ros message to cv image
        compressed_in = np.frombuffer(msg.data, np.uint8)
        frame         = cv2.imdecode(compressed_in, cv2.IMREAD_COLOR)

        # Then we can play with opencv
        analysis = vs.vanishing_point.vanishing_pt(frame)
        vp_y = analysis[0]
        img_out = analysis[1]
        unbalance = analysis[2]

        # Let us publish the treated image
        if self.pub_image.get_num_connections() > 0:
            msg              = CompressedImage()
            msg.header.stamp = rospy.Time.now()
            msg.format       = "jpeg"
            msg.data         = np.array(cv2.imencode('.jpg', img_out)[1]).tobytes()
            self.pub_image.publish(msg)

        if self.pub_vp.get_num_connections() > 0:
            msg              = Float32()
            #msg.header.stamp = rospy.Time.now()
            #msg.format       = "Float32"
            msg.data         = vp_y
            self.pub_vp.publish(msg)

        if self.pub_centre.get_num_connections() > 0:
            msg              = Float32()
            #msg.header.stamp = rospy.Time.now()
            #msg.format       = "Float32"
            msg.data         = unbalance
            self.pub_centre.publish(msg)

if __name__ == '__main__':     # This is the main thread, thread #1
    rospy.init_node('my_node')

    my_node = MyNode()   
    rospy.spin() # useless... since loop already blocks. If you have
                 # no idle job (i.e. loop) to do outside event
                 # handling, rospy.spin() is mandatory in order to
                 # prevent from immediate termination of your node.ect a variable by a mutex...