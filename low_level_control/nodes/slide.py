#!/usr/bin/env python
import rospy
import time
import numpy as np
from behavior import behaviour
from behavior.msg import BehaviorStatus
from std_msgs.msg import Float32
from multiprocessing import Lock
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Pose
from nav_msgs.msg import Odometry
from sympy import conjugate
from sympy.algebras.quaternion import Quaternion
from geometry_msgs.msg import Point
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point


class Slide(behaviour.Behavior) :

    def __init__(self, name, rotating_direction) :
        super().__init__(name)
        # parameters
        self.odom_pose = Pose()
        self.vector = list() 
        self.dist = Float32() 
        self.theta = Float32()
        #gains for the closed loop
        self.kx = 0.5
        self.kz = 0.5
        #linear_y and angular_z speeds to perform an open loop slide on the desired direction
        self.msg_linear_y = Float32(-rotating_direction * 0.5)
        self.msg_angular_z = Float32(rotating_direction * 2.5)
        

        #Publishers
        self.linear_x_pub = rospy.Publisher("/linear_x", Float32, queue_size=1)
        self.linear_y_pub = rospy.Publisher("/linear_y", Float32, queue_size=1)
        self.angular_z_pub = rospy.Publisher("/angular_z", Float32, queue_size=1)
        self.pub_axis     = rospy.Publisher("slide_axis", Marker, queue_size=1)

        #Subscriber
        #self.odom_sub = rospy.Subscriber("/bebop/odom", Odometry, self.slide, queue_size=1)
        
    def on_status_on(self):

        #go along the x axis by 0.5m/s for 2s
        msg_linear_x = Float32()
        msg_linear_x.data = 0.5
        self.linear_x_pub.publish(msg_linear_x)
        time.sleep(2)
        self.linear_x_pub.publish(0)

        #turn left/right by 2.5rad/s for 1s
        self.angular_z_pub.publish(self.msg_angular_z)
        time.sleep(1)
        msg_angular_z.data = 0
        self.angular_z_pub.publish(msg_angular_z)
        
        #go along the y axis by 0.5m/s
        self.linear_y_pub.publish(self.msg_linear_y)
        """

    def slide(self, msg) :

        self.odom_pose = msg.pose.pose
        x = self.odom_pose.position.x
        y = self.odom_pose.position.y
        z = self.odom_pose.position.z
        OP0 = [x,y,z,0]
        rospy.loginfo('pose initiale')
        rospy.loginfo(self.odom_pose)
        rospy.loginfo(OP0)

        
        qx = self.odom_pose.orientation.x 
        qy = self.odom_pose.orientation.y  
        qz = self.odom_pose.orientation.z  
        qw = self.odom_pose.orientation.w 

        q1 = Quaternion(qw, qx, qy, qz)
        v = Quaternion(0, 1, 0, 0).normalize()


        res = q1 * v * conjugate(q1)
        self.v_vec = Quaternion(res.a, res.b, res.c, 0.0).normalize()

        

        

        marker = Marker()
        marker.header.frame_id = "odom" # Do not use / in frame names !
        marker.type = marker.LINE_STRIP
        marker.lifetime = rospy.Duration(secs=1000)

        # marker scale
        marker.scale.x = 0.03
        marker.scale.y = 0.03
        marker.scale.z = 0.03

        # marker color
        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 0.0

        # marker orientaiton
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 0.0
        marker.pose.orientation.w = 1.0

        # marker position
        marker.pose.position.x = 0.0
        marker.pose.position.y = 0.0
        marker.pose.position.z = 0.0

        # marker line points
        marker.points = []
        point = Point()
        point.x = x - 10 * qx
        point.y = y - 10 * qy
        point.z = z - 10 * qz
        marker.points.append(point)

        point = Point()
        point.x = x+1 + 10 * qx
        point.y = y + 10 * qy
        point.z = z + 10 * qz

        marker.points.append(point)

        self.pub_axis.publish(marker)

        OD_vec = [self.odom_pose.position.x,self.odom_pose.position.y,self.odom_pose.position.z,0]
        rospy.loginfo(OD_vec)
        PD_vec = np.dot(np.eye(4) - np.dot(self.v_vec.transpose(), self.v_vec))*(OD_vec.transpose() - OP0.T)
        #calcul de la distance 
        self.dist = sign(np.dot(PD_vec, v)) * np.linalg.norm(PD_vec)
        #calculde theta
        cos_theta = np.dot(normal, v)
        sin_theta = np.linalg.norm(np.cross(normal, v))
        self.theta = np.atan2(cos_theta, sin_theta)


        vel_linear_x = kx* self.dist
        vel_angular_z = kz * self.theta

        self.linear_x_pub.publish(vel_linear_x)
        self.angular_z_pub.publish(vel_angular_z)"""


if __name__ == '__main__':
    rospy.init_node('slideLeft')
    slideRight = Slide("slideLeft",1)
    rospy.spin()
    