#!/usr/bin/env python3

'''
Ce noeud contient deux classes :
- La classe TeleopJoy : calcule les commandes de vitesse à partir des données du message envoyé par le joystick
via ses deux analogues: l'analogue gauche contrôle le roulis et le tangage et l'analogue droit contrôle
la vitesse du lacet et la vitesse verticale.

- La classe JoyTeleop qui envoie en fonction des boutons pressés les commandes sur le topic "/command" au noeud
command.py : c'est la classe utilisée pour la navigation semi-autonome

'''


import time
import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from std_msgs.msg import Empty, Float32, Bool, String
import math

class TeleopJoy(object):

    def __init__(self):
        self.Left_analogue_x = 1
        self.Left_analogue_y = 0
        self.Right_analogue_linear_z = 4
        self.Right_analogue_angular_z = 3
        self.RT_BUTTON = 5 # le button de decollage du joystick
        self.previous_state_RT = False 


        self.linear_x_pub = rospy.Publisher('/linear_x', Float32, queue_size=1)
        self.linear_y_pub = rospy.Publisher('/linear_y', Float32, queue_size=1)
        self.linear_z_pub = rospy.Publisher('/linear_z', Float32, queue_size=1)
        self.angular_z_pub = rospy.Publisher('/angular_z', Float32, queue_size=1)
        #self.cmd_vel_pub = rospy.Publisher('bebop/cmd_vel', Twist, queue_size=1)
        self.takeoff_pub = rospy.Publisher('/bebop/takeoff', Empty, queue_size=1)
        self.land_pub = rospy.Publisher('/bebop/land', Empty, queue_size=1)


        self.joy_sub = rospy.Subscriber('/joy', Joy, self.joy_cb)

    def joy_cb(self, msg):

        
        if msg.buttons[self.RT_BUTTON] and not self.previous_state_RT:  
            print('callback de teleopjoy')
            self.previous_state_RT = True
            self.takeoff_pub.publish(Empty())
        
        if not msg.buttons[self.RT_BUTTON] and self.previous_state_RT: 
            self.previous_state_RT = False
            self.land_pub.publish(Empty())


        
        if msg.buttons[self.RT_BUTTON]: 
            
            # Publie les vitesses 
            self.linear_x_pub.publish(msg.axes[self.Left_analogue_x])
            self.linear_y_pub.publish(msg.axes[self.Left_analogue_y])
            self.linear_z_pub.publish(msg.axes[self.Right_analogue_linear_z])
            print("z_pub", msg.axes[self.Right_analogue_angular_z])
            self.angular_z_pub.publish(msg.axes[self.Right_analogue_angular_z] )
            cmd_vel_msg = Twist()
            cmd_vel_msg.linear.x = msg.axes[self.Left_analogue_x]
            cmd_vel_msg.linear.y = msg.axes[self.Left_analogue_y]
            cmd_vel_msg.linear.z = msg.axes[self.Right_analogue_linear_z]
            cmd_vel_msg.angular.z = msg.axes[self.Right_analogue_angular_z]
            #self.cmd_vel_pub.publish(cmd_vel_msg)
            print('le message ', cmd_vel_msg)

        else :
            print(" je ne recois rien du joystick")




if __name__ == '__main__':
    rospy.init_node('TeleopJoy')
    teleopJoy = TeleopJoy()
    rospy.spin()

