#!/usr/bin/env python
import rospy
import time
from behavior import behaviour
from behavior.msg import BehaviorStatus
from std_msgs.msg import Float32
from multiprocessing import Lock

class u_turn(behaviour.Behavior) :

    def __init__(self):
        super().__init__("Uturn")
        #publisher
        self.target_vel_pub = rospy.Publisher('/angular_z', Float32, queue_size=1)


    def on_status_on(self) :
        rospy.loginfo("On status on !!!!")
        self.target_vel_pub.publish(Float32(0.32))
        time.sleep(3)
        self.target_vel_pub.publish(Float32(0.0))
        self.set_status(False)

        




   

if __name__=='__main__':

    rospy.init_node("u_turn")
    u_turn = u_turn()
    rospy.spin()
