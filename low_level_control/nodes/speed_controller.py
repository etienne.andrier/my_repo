#!/usr/bin/env python3

"""
This node transforms the linear and angular velocities into the commands which are expected 
by the drone

It Subscribe to :
    - linear x : speed request on the x axis
    - linear y : speed request on the y axis
    - linear z : speed on the z axis
    - odom : give the coordinates of the drone

It Publishes to :
    - cmd_vel
    - target_vel

"""

import datetime
import rospy
from std_msgs.msg import Float32, Bool, Empty
from sensor_msgs.msg import Joy, CompressedImage
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from multiprocessing import Lock


class SpeedController :

    def __init__(self) :


        #parameters
        '''
        Parametres de Ziegler-Nichols de reglage du controlleur PID
        PID classique
        '''
        self.ku = 0.6
        self.Tu = 0.005
        self.Kp = 0.6*self.ku
        self.Kd = 0.075*self.ku*self.Tu
        self.Ki = 1.2*self.ku/self.Tu 

        self.request_vel = Twist() # les commandes de vitesse envoyées par le joystick via le noeud teleop 
        self.command_vel = Twist() # la consigne
        self.vel_estimation = Twist() # vitesse du robot mesuré via l'odometry

        self.pid_errors = { 
        "linear_x" : {'error' : 0, 'D_error': 0, 'I_error' : 0, 'time' : None}, 
        "linear_y": { 'error':0, 'D_error' : 0, 'I_error' : 0, 'time' : None}
        }

        self.gains = {
                "linear_x":   {'Kp': self.Kp, 'Kd': 0, 'Ki': self.Ki},
                "linear_y":  {'Kp': self.Kp, 'Kd': 0, 'Ki': self.Ki}
        }

        self.time = None
        # protection of the access to the stored velocity
        self.mutex = Lock()

        self.hover_bool = False
        self.bool_publish_x = False
        self.bool_publish_y = False
        # Pulisher
        self.cmd_vel_pub = rospy.Publisher("/bebop/cmd_vel", Twist, queue_size=1)
        self.target_vel_pub = rospy.Publisher("/target_vel", Twist, queue_size=1)

        # Subscriber
        self.odometry = rospy.Subscriber('/bebop/odom', Odometry, self.on_odom, queue_size=1)
        self.linear_x = rospy.Subscriber('/linear_x', Float32, self.x_cb, queue_size=1)
        self.linear_y = rospy.Subscriber('/linear_y', Float32, self.y_cb,queue_size=1)
        self.linear_z_pub = rospy.Subscriber('/linear_z', Float32,self.linear_z_cb , queue_size=1)
        self.angular_z_pub = rospy.Subscriber('/angular_z', Float32, self.angular_z_cb, queue_size=1)
        self.hover = rospy.Subscriber('/hover', Empty, self.hover_cb, queue_size=1)

    # callback for linear x
    def x_cb(self, msg) :
        with self.mutex :
            self.request_vel.linear.x = msg.data
            self.hover_bool = False
            self.bool_publish_x = (self.request_vel.linear.x!=0)
    # callback for linear y
    def y_cb(self,msg) :
        with self.mutex :
            self.request_vel.linear.y = msg.data
            self.hover_bool = False
            self.bool_publish_y = (self.request_vel.linear.y!=0)
    # callback for linear z
    def linear_z_cb(self, msg) :
        with self.mutex :
            self.command_vel.linear.z = msg.data
            self.hover_bool = False
    # callback for angular z
    def angular_z_cb(self, msg):
        with self.mutex :
            self.command_vel.angular.z = msg.data
            self.hover_bool = False
    def hover_cb(self, msg) :
        with self.mutex :
            self.hover_bool = True

    # controlleur PID pour ajuster les vitesses de roulis et de tangage
    def on_odom(self, msg) :
        with self.mutex :
            self.vel_estimation = msg.twist.twist
            self.time = msg.header.stamp

        #PID
        err_x = self.pid_errors["linear_x"]["error"]
        err_y = self.pid_errors["linear_y"]["error"]
        dt = 0.2 # periode de transmission des données de l'odometrie (frequence 5hz)

        # calcul des vitesses suivant x et y en fonction des erreurs et des gains
        print("hover value", self.hover_bool)
        if not self.hover_bool :
            print("hover off")
            self.pid_errors["linear_x"]["error"] = self.request_vel.linear.x - self.vel_estimation.linear.x 
            self.pid_errors["linear_x"]["D_error"] = (self.pid_errors["linear_x"]["error"] - err_x) / dt 
            self.pid_errors["linear_x"]["I_error"] += self.pid_errors["linear_x"]["error"] * dt 

            self.pid_errors["linear_y"]["error"] = self.request_vel.linear.y - self.vel_estimation.linear.y  
            self.pid_errors["linear_y"]["D_error"] = (self.pid_errors["linear_y"]["error"] - err_y) / dt 
            self.pid_errors["linear_y"]["I_error"] += self.pid_errors["linear_y"]["error"] * dt
            if self.bool_publish_x :
                self.command_vel.linear.x = self.gains["linear_x"]["Kp"] * self.pid_errors["linear_x"]["error"] + \
                                            self.gains["linear_x"]["Kd"] * self.pid_errors["linear_x"]["D_error"] + \
                                            self.pid_errors["linear_x"]["I_error"] / self.gains["linear_x"]["Ki"]

            else :
                self.command_vel.linear.x = 0

            if self.bool_publish_y :
                self.command_vel.linear.y = self.gains["linear_y"]["Kp"] * self.pid_errors["linear_y"]["error"] + \
                                            self.gains["linear_y"]["Kd"] * self.pid_errors["linear_y"]["D_error"] + \
                                            self.pid_errors["linear_y"]["I_error"] / self.gains["linear_y"]["Ki"]
            else :
                self.command_vel.linear.y = 0

        else :
            print("hover on")
            self.pid_errors = { 
            "linear_x" : {'error' : 0, 'D_error': 0, 'I_error' : 0, 'time' : None}, 
            "linear_y": { 'error':0, 'D_error' : 0, 'I_error' : 0, 'time' : None}
            }
            self.command_vel = Twist()
            self.request_vel = Twist()

        # publish velocities
        self.cmd_vel_pub.publish(self.command_vel)
        self.target_vel_pub.publish(self.command_vel)


if __name__ == '__main__' :
    rospy.init_node('speed_controller')
    node = SpeedController()
    rospy.spin()