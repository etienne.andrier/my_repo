#!/usr/bin/env python3

#Classe intermédiaire qui hérite de Behavior
#Et dont tous les autres fichiers qui permettent un déplacement hériteront

import rospy
from behavior import behaviour
from std_msgs.msg import Empty, Float32


#constant speed en m/s
CONST_SPEED = 0.7
#constant angular speed en rad/s
CONST_ANG_SPEED = 0.4


"""
Classe intermédiaire que nous utiliserons pour avoir un code plus intelligent dans la partie "Direct"
Cela nous permet de factoriser le code sans avoir à l'écrire à chaque fois
Les fichiers Forward, Backward, Left, Right, TurnLeft, TurnRight, Up, Down s'écriront très rapidement grace à cette classe
"""
class ConstantScalar(behaviour.Behavior):

    def __init__(self, behavior_name, topic_name, value):
        super().__init__(behavior_name)
        self.value = value
        #Publisher
        self.publisher_value = rospy.Publisher(topic_name, Float32, queue_size = 1)

    def on_status_on(self):
        #Lorsque le Behavior est activé, on publie
        #sur le topic takeoff du drone
        message = self.value
        self.publisher_value.publish(message)

"""
if __name__='__main__' :
    rospy.init_node('land')
    land = Land()
    rospy.spin()
"""
