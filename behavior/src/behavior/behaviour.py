#!/usr/bin/env python3

import rospy

# import the message type you need
from behavior.msg import BehaviorStatus

# If you need to protect a variable by a mutex...
from multiprocessing import Lock

"""
Classe pour implémenter ce qu'est un behavior
Tout le reste de notre travail consistera principalement à hériter de cette classe
"""
class Behavior :
    def __init__(self, name):
        self.status = False
        self.name = name
        self.status_mutex = Lock() # this will be thread protected thanks to this mutex.
        #Publisher
        self.pub = rospy.Publisher('behaviors_status', BehaviorStatus, queue_size = 1)
        #Subscriber
        self.sub = rospy.Subscriber('behavior', BehaviorStatus, self.callback, queue_size = 1)


    def callback(self, msg):
        rospy.loginfo(rospy.get_caller_id() + " got a behavior message")
        if(msg.name == self.name):
            self.set_status(msg.status)
            rospy.loginfo(f"{rospy.get_caller_id()} : The status is {self.status}")
        elif(msg.name == "ping"):
            status_message = BehaviorStatus()
            status_message.name = self.name
            status_message.status = self.status
            self.pub.publish(status_message)
            rospy.loginfo(f"{rospy.get_caller_id()} : The status is {self.status}")
    
    
    # Getter pour obtenir le status
    def get_status(self) :
        with self.status_mutex :
            copy = self.status
        return copy


    # Setter pour modifier le status
    def set_status(self, new_status):
        with self.status_mutex:
            self.status = new_status
        if new_status:
            self.on_status_on()
        else:
            self.on_status_off()

    
    def on_status_on(self) :
        pass
    
    def on_status_off(self) :
        pass
        


"""
if __name__ == '__main__':     # This is the main thread, thread #1
    rospy.init_node('behavior')
    behavior = Behavior()
    rospy.spin() # useless... since loop already blocks. If you have
                 # no idle job (i.e. loop) to do outside event
                 # handling, rospy.spin() is mandatory in order to
                 # prevent from immediate termination of your node.
"""
