#!/usr/bin/env python3

import rospy
from behavior import behaviour
from behavior import constantscalar
from std_msgs.msg import Float32


"""
Classe pour faire monter (en altitude) le drone
"""
class Up(constantscalar.ConstantScalar):
    def __init__(self):
        super().__init__('Up', '/linear_z', constantscalar.CONST_SPEED)


if __name__ == '__main__' :
    rospy.init_node('up')
    up = Up()
    rospy.spin()
