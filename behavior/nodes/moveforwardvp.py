#!/usr/bin/env python3
import rospy
from behavior import behaviour
from std_msgs.msg import Empty, Float32, String

speed = 0.4 #m/s


"""
Classe pour faire avancer le drone si un vanishing point est détecté
"""
class MoveForwardVp(behaviour.Behavior):
    def __init__(self):
        super().__init__('MoveForwardVp')
        #Publisher
        self.pub_linear_x = rospy.Publisher('/linear_x', Float32, queue_size = 1)
        #Subscriber
        self.sub_vanishingpoint_linearx = rospy.Subscriber('/vanishing_point_y', Float32, self.callback_speed_x, queue_size = 1)

    def callback_speed_x(self, msg):
        if self.get_status():
            #egal 100 si pas de vanishing point
            #Donc ici on a un vanishing point
            if msg.data != 100:
                self.pub_linear_x.publish(speed)
            #Ici pas de vanishing point
            else:
                self.pub_linear_x.publish(0)


if __name__ == '__main__' :
    rospy.init_node('moveForwardVp')
    moveForwardVp = MoveForwardVp()
    rospy.spin()

