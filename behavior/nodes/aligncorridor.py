#!/usr/bin/env python3
import rospy
from behavior import behaviour
from std_msgs.msg import Empty, Float32, String

omega_max = 0.5 #rad/s


"""
Classe pour aligner le drone dans l'axe d'un couloir grace au vanishing point
"""
class AlignCorridor(behaviour.Behavior):
    def __init__(self):
        super().__init__('AlignCorridor')
        #Publisher
        self.pub_angularz = rospy.Publisher('/angular_z', Float32, queue_size = 1)
        #Subscriber
        self.sub_vanishingpoint_align = rospy.Subscriber('/vanishing_point_y', Float32, self.callback_omega, queue_size = 1)

    def callback_omega(self, msg):
        if self.get_status():
            #egal 100 si pas de vanishing point
            #Donc ici on a un vanishing point
            if msg.data != 100:
                ang_speed = -msg.data * omega_max
                self.pub_angularz.publish(ang_speed)
            #Ici pas de vanishing point
            else:
                ang_speed = 0
                self.pub_angularz.publish(ang_speed)


if __name__ == '__main__' :
    rospy.init_node('alignCorridor')
    alignCorridor = AlignCorridor()
    rospy.spin()

