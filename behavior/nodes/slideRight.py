#!/usr/bin/env python3
import rospy
from low_level_control.nodes import slide


"""
Classe pour faire slide à droite le drone
"""
class SlideRight(slide.Slide):
    def __init__(self):
        super().__init__("SlideRight", -1)

if __name__ == '__main__':
    rospy.init_node('slideRight')
    slideRight = SlideRight()
    rospy.spin()
