#!/usr/bin/env python3

import rospy
from behavior import behaviour
from behavior import constantscalar
from std_msgs.msg import Float32


"""
Classe pour faire tourner à droite le drone
"""
class TurnRight(constantscalar.ConstantScalar):
    def __init__(self):
        super().__init__('TurnRight', '/angular_z', -constantscalar.CONST_ANG_SPEED)


if __name__ == '__main__' :
    rospy.init_node('turnright')
    turnright = TurnRight()
    rospy.spin()
