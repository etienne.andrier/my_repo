#!/usr/bin/env python3

import rospy
from behavior import behaviour
from behavior import constantscalar
from std_msgs.msg import Float32


"""
Classe pour faire aller à gauche le drone
"""
class Left(constantscalar.ConstantScalar):
    def __init__(self):
        super().__init__('Left', '/linear_y', constantscalar.CONST_SPEED)


if __name__ == '__main__' :
    rospy.init_node('left')
    left = Left()
    rospy.spin()