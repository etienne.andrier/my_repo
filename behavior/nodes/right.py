#!/usr/bin/env python3

import rospy
from behavior import behaviour
from behavior import constantscalar
from std_msgs.msg import Float32


"""
Classe pour faire aller à droite le drone
"""
class Right(constantscalar.ConstantScalar):
    def __init__(self):
        super().__init__('Right', '/linear_y', -constantscalar.CONST_SPEED)


if __name__ == '__main__' :
    rospy.init_node('right')
    right = Right()
    rospy.spin()