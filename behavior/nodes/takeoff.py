#!/usr/bin/env python3

import rospy
from behavior import behaviour
from std_msgs.msg import Empty
import time


"""
Classe pour faire s'envoler le drone
"""
class TakeOff(behaviour.Behavior):
    def __init__(self):
        super().__init__('TakeOff')
        #Publisher
        self.pub_takeoff = rospy.Publisher('/takeoff', Empty, queue_size = 1)

    def on_status_on(self):
        #Lorsque le Behavior est activé, on publie
        #sur le topic takeoff du drone
        message = Empty()
        self.pub_takeoff.publish(message)

        #we wait a bit (i.e 2 seconds)
        time.sleep(2)
        self.set_status(False)

if __name__ == '__main__' :
    rospy.init_node('takeOff')
    takeOff = TakeOff()
    rospy.spin()

