#!/usr/bin/env python3

import rospy
from behavior import behaviour
from behavior import constantscalar

from std_msgs.msg import Float32


"""
Classe pour faire avancer le drone
"""
class Forward(constantscalar.ConstantScalar):
    def __init__(self):
        super().__init__('Forward', '/linear_x', constantscalar.CONST_SPEED)


if __name__ == '__main__' :
    rospy.init_node('forward')
    forward = Forward()
    rospy.spin()