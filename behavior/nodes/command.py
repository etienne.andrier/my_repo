#!/usr/bin/env python3

import rospy
import time
from behavior import behaviour
from behavior.msg import BehaviorStatus
from std_msgs.msg import String
from multiprocessing import Lock


"""
Classe pour créer des commandes à affecter à des touches du joystick
Elles permettent les mouvements du drone et disent quand ils ont lieu
"""
class Command:
    def __init__(self):
        self.behaviors = ['Hover', 'Left', 'Right', 'Up', 'Down', 'Land', 'TakeOff', 'Forward', 'Backward', 'TurnLeft', 'TurnRight', 'AlignCorridor', 'CenterCorridor', 'MoveForwardVp', 'Uturn', 'SlideLeft', 'SlideRight', 'Stairs']
        self.pub_behavior = rospy.Publisher('behavior', BehaviorStatus, queue_size = 1)
        self.commands = {
            'Stop' : [(0, 'Hover')],
            'Dance' : [(0, 'Left'), (0, 'Up'), (2.5, 'Right'), (3.2, 'Hover')],
            'TakeOff' : [(0, 'TakeOff'), (1, 'Hover')],
            'Hover' : [(0, 'Hover')],
            'Land' : [(0, 'Hover'), (0.3, 'Land')],
            'Left' : [(0, 'Left')],
            'Right' : [(0, 'Right')], 
            'Up' : [(0, 'Up')],
            'Down' : [(0, 'Down')],
            'Forward' : [(0, 'Forward')],
            'Backward' : [(0, 'Backward')],
            'TurnLeft' : [(0, 'TurnLeft')],
            'TurnRight' : [(0, 'TurnRight')],
            'AlignCorridor' : [(0, 'AlignCorridor')],
            'CenterCorridor' : [(0, 'CenterCorridor')],
            'MoveForwardVp' : [(0, 'MoveForwardVp')],
            'Test' : [(0, 'CenterCorridor'), (0, 'AlignCorridor')],
            'GoAhead' : [(0, 'AlignCorridor'), (0, 'CenterCorridor'), (1, 'MoveForwardVp')],
            'UTurn' : [(0, 'Uturn')],
            'TurnBack' : [(0, 'Uturn'), (4, 'AlignCorridor'), (4, 'CenterCorridor'), (4, 'MoveForwardVp')],
            'SlideLeft' : [(0, 'SlideLeft'), (3, 'AlignCorridor'), (3, 'CenterCorridor'), (4, 'MoveForwardVp')],
            'SlideRight' : [(0, 'SlideRight'), (3, 'AlignCorridor'), (3, 'CenterCorridor'), (4, 'MoveForwardVp')],
            'Stairs' : [(0, 'Stairs')]
            }


        self.mutex = Lock()
        self.sub_command = rospy.Subscriber('command', String, self.callback, queue_size = 1)
        self.time_behavior_set = []


    
    def callback(self, msg):
        #Desac tous les behaviors qd on rentre ici
        for behavior in self.behaviors:
            bhStatus = BehaviorStatus()
            bhStatus.name = behavior
            bhStatus.status = False #C'est ici que je desac tout
            self.pub_behavior.publish(bhStatus)

        #tuple (temps, comportement)
        commandes_msg = self.commands[msg.data]


        #Idée pour le temps : on commence à un temps initial (maintenant)
        #On sait que chaque commande commence au temps initial + la fin de la commande d'avant
        now = rospy.get_rostime().secs


        with self.mutex:
            self.time_behavior_set = [(now + c[0], c[1]) for c in commandes_msg]




    def loop(self):
        while not rospy.is_shutdown():

        #activer toutes les commandes passées par rapport à mtn et les supprimer de la liste
            with self.mutex:
                if len(self.time_behavior_set) != 0:
                        self.actual_behavior = self.time_behavior_set[0]
                        if(rospy.get_rostime().secs >= self.actual_behavior[0]):
                            message = BehaviorStatus()
                            message.name = self.actual_behavior[1]
                            message.status = True #On active le behavior dont le temps est antérieur à l'instant actuel
                            self.pub_behavior.publish(message)
                            self.time_behavior_set.pop(0)


if __name__ == '__main__':     # This is the main thread
    rospy.init_node('command')
    command = Command()
    command.loop()
    rospy.spin()