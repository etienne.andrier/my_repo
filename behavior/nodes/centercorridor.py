#!/usr/bin/env python3
import rospy
from behavior import behaviour
from std_msgs.msg import Empty, Float32, String

speed_max = 0.8 #m/s


"""
Classe pour centrer le drone dans un couloir grace au vanishing point
"""
class CenterCorridor(behaviour.Behavior):
    def __init__(self):
        super().__init__('CenterCorridor')
        #Publisher
        self.pub_linear_y = rospy.Publisher('/linear_y', Float32, queue_size = 1)
        #Subscriber
        self.sub_vanishingpoint_centrism = rospy.Subscriber('/centrism', Float32, self.callback_speed_y, queue_size = 1)

    def callback_speed_y(self, msg):
        if self.get_status():
            #egal 100 si pas de vanishing point
            #Donc ici on a un vanishing point
            if msg.data != 100:
                speed = -msg.data * speed_max
                self.pub_linear_y.publish(speed)
            #Ici pas de vanishing point
            else:
                speed = 0
                self.pub_linear_y.publish(speed)


if __name__ == '__main__' :
    rospy.init_node('centerCorridor')
    centerCorridor = CenterCorridor()
    rospy.spin()