#!/usr/bin/env python3

import rospy
from behavior import behaviour
from std_msgs.msg import Empty
import time


"""
Classe pour faire atterir le drone
"""
class Land(behaviour.Behavior):
    def __init__(self):
        super().__init__('Land')
        #Publisher
        self.pub_land = rospy.Publisher('/land', Empty, queue_size = 1)

    def on_status_on(self):
        #Lorsque le Behavior est activé, on publie
        #sur le topic takeoff du drone
        message = Empty()
        self.pub_land.publish(message)

        #we wait a bit (i.e 2 second)
        time.sleep(2)
        self.set_status(False)

if __name__ == '__main__' :
    rospy.init_node('land')
    land = Land()
    rospy.spin()
