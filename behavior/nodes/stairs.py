#!/usr/bin/env python3

import rospy
from behavior import behaviour
from std_msgs.msg import Empty, Float32, String

class Stairs(behaviour.Behavior):
    def __init__(self):
        super().__init__("Stairs")
        #Publisher sur l'axe x
        self.pub_on_x = rospy.Publisher("/linear_x", Float32, queue_size = 1)
        #Publisher sur l'axe z
        self.ub_on_z = rospy.Publisher("/linear_z", Float32, queue_size = 1)
        #Subscriber sur le vanishing point
        self.sub_on_vanishpt = rospy.Subscriber("/vanishing_point_y", Float32, self.callback_vanishpt, queue_size = 1)


        def callback_vanishpt(self, msg):
            #Intuition : quand un vanishing point est détecté, on publie vitesse non nulle sur /linear_x
            #Sinon on publie vitesse nulle

            if self.get_status():
                #Si un vanishing point est détecté
                if msg.data != 100:
                    pass #J'y réfléchirai plus tard
                else:
                    pass #j'y réfléchirai aussi plus tard


if __name__ == '__main__':
    rospy.init_node('stairs')
    stairs = Stairs()
    rospy.spin()


