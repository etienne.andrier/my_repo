#!/usr/bin/env python3

import rospy
from behavior import behaviour
from behavior import constantscalar

from std_msgs.msg import Float32


"""
Classe pour faire reculer le drone
"""
class Backward(constantscalar.ConstantScalar):
    def __init__(self):
        super().__init__('Backward', '/linear_x', -constantscalar.CONST_SPEED)


if __name__ == '__main__' :
    rospy.init_node('backward')
    backward = Backward()
    rospy.spin()