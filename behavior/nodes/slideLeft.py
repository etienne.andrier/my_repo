#!/usr/bin/env python3

import rospy
from low_level_control.nodes import slide


"""
Classe pour faire slide à gauche le drone
"""
class SlideLeft(slide.Slide):
    def __init__(self):
        super().__init__("SlideLeft", 1)

if __name__ == '__main__':
    rospy.init_node('slideLeft')
    slideLeft = SlideLeft()
    rospy.spin()