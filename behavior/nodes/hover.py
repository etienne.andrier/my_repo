#!/usr/bin/env python3

import rospy
from behavior import behaviour
from std_msgs.msg import Empty
import time


"""
Classe pour arrêter toutes les commandes effectuées par le drone
"""
class Hover(behaviour.Behavior):
    def __init__(self):
        super().__init__('Hover')
        #Publisher
        self.pub_hover = rospy.Publisher('/hover', Empty, queue_size = 1)


    def on_status_on(self):
        message = Empty()
        self.pub_hover.publish(message)

        #we wait a bit (i.e 2 seconds)
        time.sleep(2)
        self.set_status(False)


if __name__ == '__main__':
    rospy.init_node('hover')
    hover = Hover()
    rospy.spin()